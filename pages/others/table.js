import React, { useState, useEffect } from 'react';

import Link from 'next/link'
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { fetchMovies } from "../../services/moviesService";

const Table = () => {
  const columns = [
    { field: 'movie_theater', header: 'Movie Theater' },
    { field: 'movie', header: 'Movie' },
    { field: 'time', header: 'Time' }
  ];

  const [movies, setMovies] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await fetchMovies();
      setMovies(result)
    }
    fetchData();
  }, [])

  const dynamicColumns = columns.map((col, i) => {
    return <Column key={col.field} field={col.field} header={col.header} />;
  });

  return (
    <div className="content-section implementation">
      <h3>Dynamic Columns</h3>
      <DataTable value={movies}>
        {dynamicColumns}
      </DataTable>
      <div>
        <Link href="/">
          <a>Back home</a>
        </Link>
      </div>
    </div>
  );
}

export default Table;
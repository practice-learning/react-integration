import React, { useState } from 'react';

import Link from 'next/link'
import { Button } from 'primereact/button';

const themeContext = () => {
    const [theme, setTheme] = useState("red");

    const onClickHandler = () => {
        setTheme(theme == "red" ? "blue" : "red");
    }

    return (
        <div>
            <h1 style={{ color: `${theme}` }}>
                {theme}
            </h1>
            <Button onClick={onClickHandler} label="Change theme" />
            <div>
                <Link href="/">
                    <a>Back home</a>
                </Link>
            </div >
        </div >
    )
}

export default themeContext;
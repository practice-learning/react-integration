import React, { useState } from 'react';

import Link from 'next/link'
import { FileUpload } from 'primereact/fileupload';
import { Growl } from 'primereact/growl';

const PurchaseUploadForm = () => {

	const [growl, setGrowl] = useState("");

	const onUpload = (event) => {
		growl.show({
			severity: 'info',
			summary: 'Success',
			detail: 'File Uploaded'
		});
	}

	return (
		<div>
			<div className='content-section implementation'>
				<h3>Advanced</h3>
				<FileUpload
					mode="basic"
					name='file'
					url='http://localhost:8000/upload'
					onUpload={onUpload}
					multiple={true}
					accept='.xml'
					maxFileSize={1000000}
				/>
				<Growl
					ref={el => {
						setGrowl(el);
					}}
				/>
				<div>
					<Link href="/">
						<a>Back home</a>
					</Link>
				</div>
			</div>
		</div>
	)
}

export default PurchaseUploadForm;
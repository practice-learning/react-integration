import React, { useState } from 'react';

import Link from 'next/link'

import KeyPadComponent from '../../components/KeyPadComponent';
import ResultComponent from '../../components/ResultComponent';

import { calculate, reset, blancspace } from "../../methods/operations";

const Calculator = () => {

    const [result, setResult] = useState("");

    const onClick = button => {
        //setResult(button);
        if (button === "=") {
            setResult(calculate(result));
        }

        else if (button === "C") {
            setResult(reset());
        }

        else if (button === "CE") {
            setResult(blancspace(result));
        }

        else {
            setResult(result + button);
        }
    }

    return (
        <div>
            <h1>Calculator</h1>
            <ResultComponent result={result} />
            <KeyPadComponent onClick={(e) => onClick(e)} />
            <div>
                <Link href="/">
                    <a>Back home</a>
                </Link>
            </div>
        </div>
    )
}

export default Calculator;
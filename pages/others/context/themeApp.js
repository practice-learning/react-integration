import React, { useState } from 'react';

import ThemeContext from './themeContext'
import Link from 'next/link'

import Header from "./header";
import Main from "./MainWithFunction";


const themeApp = () => {
    const themeHook = useState("light");
    return (
        <ThemeContext.Provider value={themeHook}>
            <div>
                <Header />
                <Main />
                <div>
                    <Link href="/">
                        <a>Back home</a>
                    </Link>
                </div>
            </div>
        </ThemeContext.Provider>
    );
}

export default themeApp;
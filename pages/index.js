import Link from 'next/link'

function HomePage() {
    return (
        <div>
            <div className="App-header">
                <img src="/logo.svg" className="App-logo" alt="logo" />
                <h3>Welcome to Next.js!</h3>
            </div>
            <div>
                <Link href="/others/calculator">
                    <a>Calculator</a>
                </Link>
            </div>
            <div>
                <Link href="/others/counter">
                    <a>Counter</a>
                </Link>
            </div>
            <div>
                <Link href="/others/fileUpload">
                    <a>Upload file</a>
                </Link>
            </div>
            <div>
                <Link href="/others/table">
                    <a>Table</a>
                </Link>
            </div>
            <div>
                <Link href="/others/themeContButton">
                    <a>Context Button</a>
                </Link>
            </div>
            <div>
                <Link href="/others/context/themeApp">
                    <a>Context</a>
                </Link>
            </div>
            <div>
                <Link href="/about">
                    <a>About</a>
                </Link>
            </div>
        </div>
    )
}

export default HomePage
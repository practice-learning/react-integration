import fetch from 'unfetch'

export const fetchMovies = async () => {
  try {
    //let url = process.env.SERVICE;
    const path = "http://localhost:8000/feed";

    const res = await fetch(path);

    /*const response = await fetch(
     ***'http://localhost:8000/feed',
       *{
       *  method: "GET",
       *  mode: "cors",
       *  cache: "no-cache",
       *  headers: {
       *    "Content-Type": "application/json"
       *  }
       *}
      );*/

    const json = await res.json();
    console.log(json);
    return json;

  } catch (err) {
    console.log("something")
    console.log(err.message);
    throw err;
  }
}
const operations = require('./operations');

test('calculate operation 3+4=7', () => {
    expect(operations.calculate("3+4").toBe(7));
})